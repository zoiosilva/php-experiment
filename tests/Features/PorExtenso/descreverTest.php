<?php

use PHPUnit\Framework\TestCase;

final class PorExtenso_descrever_Test extends TestCase
{
  public function testDeve_escrever_numero_de_um_digito_por_extenso() {
    $expected = 'um';
    $value = 1;

    $actual = PorExtenso::descrever($value);

    $this->assertEquals($expected, $actual);
  }
  public function testDeve_escrever_zero_quando_passado_zero() {
    $expected = 'zero';
    $value = 0;

    $actual = PorExtenso::descrever($value);

    $this->assertEquals($expected, $actual);
  }
}
