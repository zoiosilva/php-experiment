<?php

/**
 * Descreve um número por extenso.
 */
class PorExtenso
{
  private static $zero = 'zero';
  private static $unidades = array('um', 'dois', 'três', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove',);
  private static $dezenas = array('dez', 'vinte', 'trinta', 'quarenta', 'cinquenta', 'sessenta', 'oitenta', 'noventa',);
  private static $centenas = array('cem', 'duzentos', 'trezentos', 'quatrocentos', 'quinhentos', 'seissentos', 'setecentos', 'oitocentos', 'novecentos');
  private static $milhares = array('mi', 'bi', 'tri', 'quadri', 'quinti', 'sexti', 'septi', 'octi', 'nona', 'deci',);
  private static $fator = array('lhão', 'lhões');

  /**
   * Descreve um valor numérico por extenso.
   * 
   * @param int $valor
   * @return string
   */
  public static function descrever(int $valor): string {
    if ($valor === 0) {
      return PorExtenso::$zero;
    }
    return PorExtenso::$unidades[$valor - 1];
  }
}
